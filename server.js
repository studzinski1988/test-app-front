console.log('Start node server...');

const compression = require('compression');
const express = require('express');
const app = express();
const path = require('path');
const dir = path.resolve(__dirname, 'dist');

app.use(compression());
app.use(express.static('dist', { index: false }));

app.get('*', (req, res) => {
  const filename = path.basename(req.url);
  const extension = path.extname(filename);
  const response = {
    event: 'response',
    timestamp: Date.now(),
    path: req.url
  };
  if (extension) {
    response.statusCode = 404;
    console.error(JSON.stringify(response));
    res.status(404).send('Not found');
  } else {
    response.statusCode = 200;
    console.log(JSON.stringify(response));
    res.sendFile(path.resolve(dir, 'index.html'));
  }
});

app.listen(3000, () => console.log('running on port 3000'));
