import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import config from '../../config';

import './App.css';

import { checkConnection } from '../../redux/modules/app/actionCreators';

const mapDispatchToProps = { checkConnection };

const mapStateToProps = state => ({
  response: state.app.apiResponse
});


class App extends Component {

  static propTypes = { checkConnection: PropTypes.func };

  render() {
    return (
      <div className="application">
        <Helmet>
          <title>{config.app.head.defaultTitle}</title>
        </Helmet>
        <p>Test APP</p>
        { !this.props.response && <button onClick={() => this.props.checkConnection()}>Check API Connection</button> }
        { this.props.response && <div>{this.props.response.text}</div> }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
