import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createHistory from 'history/createBrowserHistory';
import rootReducer from '../redux/reducer';

import appSaga from '../sagas/app';

const history = createHistory();
const sagaMiddleware = createSagaMiddleware();

const middleware = [
  routerMiddleware(history),
  sagaMiddleware
];

export default (initialState) => {
  const store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware)));

  if (module.hot && process.env.NODE_ENV === 'development') {
    module.hot.accept('../redux/reducer', () => store.replaceReducer(rootReducer));
  }

  if (!process.env.TESTING) {
    [appSaga].map(saga => sagaMiddleware.run(saga));
  }

  store.history = history;

  return store;
};
