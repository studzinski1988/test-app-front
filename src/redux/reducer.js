import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import app from './modules/app/reducer';

const appReducer = combineReducers({ router: routerReducer, app });

export default appReducer;
