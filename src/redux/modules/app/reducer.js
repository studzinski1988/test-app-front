import { APP } from '../../actionTypes';

export default function reducer(state = { lambdaResponse: null }, action = {}) {
  switch (action.type) {

    case APP.TEST_GET:
      return { ...state };

    case APP.TEST_GET_SUCCESS:
      return {
        ...state,
        apiResponse: action.result
      };

    case APP.TEST_GET_FAIL:
      return { ...state };

    default:
      return state;
  }
}
