import { APP } from '../../actionTypes';

export function checkConnection() {
  return {
    type: APP.TEST_GET,
    target: '/test'
  };
}
