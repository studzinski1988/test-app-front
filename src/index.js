import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import Routes from './routes';
import configureStore from './store/configureStore';
import { AppContainer } from 'react-hot-loader';

const store = configureStore();

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ConnectedRouter history={store.history}>
          <Routes />
        </ConnectedRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  );
};

render();

if (module.hot && process.env.NODE_ENV === 'development') {
  module.hot.accept('./routes', () => render());
}
