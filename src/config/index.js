require('babel-polyfill');

const merge = require('lodash/merge');

const configDev = require('./development');
const configProd = require('./production');

const environment = {
  development: Object.assign({}, configDev),
  production: Object.assign({}, configProd)
}[process.env.NODE_ENV || 'development'];

module.exports = merge({
  api: {
    host: 'faq8qmx16d.execute-api.eu-west-2.amazonaws.com/default/cems-backend',
    protocol: process.env.API_PROTOCOL ? `${process.env.API_PROTOCOL}://` : 'https://',
    timeout: '30000'
  },
  app: {
    title: 'APP FRONT',
    description: '',
    head: {
      titleTemplate: 'APP FRONT',
      meta: [
        { name: 'description', content: '' },
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=0' }
      ]
    }
  }
}, environment);

