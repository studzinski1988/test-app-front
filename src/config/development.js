require('babel-polyfill');

const TITLE = 'APP (DEVELOPMENT)';

module.exports = {
  app: {
    head: {
      defaultTitle: TITLE,
      titleTemplate: `${TITLE} - %s`
    }
  }
};
