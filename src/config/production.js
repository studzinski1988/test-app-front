require('babel-polyfill');

const TITLE = 'CEMS';

module.exports = {
  app: {
    head: {
      defaultTitle: TITLE,
      titleTemplate: `${TITLE} - %s`
    }
  }
};
