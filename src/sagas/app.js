import { takeEvery } from 'redux-saga/effects';
import major from './major';
import { APP } from '../redux/actionTypes';

function* appSaga() {
  yield takeEvery(APP.TEST_GET, major.get);
}

export default appSaga;
