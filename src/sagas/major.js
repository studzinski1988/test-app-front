import { call, put } from 'redux-saga/effects';
import 'whatwg-fetch';
import config from '../config';

const ERRORS = {
  OFFLINE: {
    errorId: 100,
    message: 'No Internet connection'
  },
  NO_API_CONNECTION: {
    errorId: 101,
    message: 'Can\'t connect to the API'
  },
  FAILED_TO_FETCH: {
    errorId: 102,
    message: 'Can\'t fetch requested URI'
  }
};

const prepareUrl = target => `${config.api.protocol}${config.api.host}/${
  target.replace(/^\//, '')}`;

export const fetchApi = target => fetch(prepareUrl(target), { mode: 'cors', timeout: 5000 })
  .then((response) => {
    return response.json();
  });

const detectError = (errorReceived) => {
  let error;
  if (!window.navigator.onLine) {
    error = { ...ERRORS.OFFLINE };
  } else if (!errorReceived) {
    error = { ...ERRORS.NO_API_CONNECTION };
  } else if (errorReceived.toString().includes('Failed to fetch')) {
    error = { ...ERRORS.FAILED_TO_FETCH };
  } else {
    error = { ...errorReceived, showDetails: true };
  }
  if (!(errorReceived && errorReceived.statusCode === 401)) {
    error.showInNotificator = true;
  }
  return { ...error, level: 'error' };
};


export default {
  get: function* get(action) {
    try {
      const result = yield call(fetchApi, action.target);
      if (!result || result.error) {
        throw result;
      }
      yield put({ type: `${action.type}_SUCCESS`, result, target: action.target, forward: action.forward });
    } catch (error) {
      yield put({
        type: `${action.type}_FAIL`, error: detectError(error), target: action.target, forward: action.forward
      });
    }
  }
};
