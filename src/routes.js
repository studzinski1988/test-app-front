import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router';
import { connect } from 'react-redux';
import App from './containers/App/App';
import './containers/App/App.css';

const mapDispatchToProps = {};

class Routes extends Component {
  render() {
    return (
      <App>
        <Switch>
          <Route exact path="/" component={App}/>
        </Switch>
      </App>
    );
  }
}

export default withRouter(
  connect(null, mapDispatchToProps)(Routes)
);
