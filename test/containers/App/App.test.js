import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from '../../../src/store/configureStore';
import App from '../../../src/containers/App/App';

describe('App container', () => {
  const store = configureStore({});
  const wrapper = mount(<Provider store={store}><App><div/></App></Provider>);

  it('should mount and be found by class application', () => {
    expect(wrapper.find('.application').length).toBe(1);
  });
});
