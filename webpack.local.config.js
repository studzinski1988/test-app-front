/* eslint-disable */
var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var config = {
    context: __dirname + '/src',

    entry: {
       index: ['babel-polyfill',
         'react-hot-loader/patch',
         'webpack-dev-server/client?http://localhost:3000',
         'webpack/hot/only-dev-server',
         '../src/index.js']
    },

    output: {
        path: __dirname + '/dist/static',
        filename: '[name].bundle.js',
        sourceMapFilename: '[name].bundle.map',
        chunkFilename: '[name].bundle.js',
        publicPath: '/static/'
    },

    resolve: {
      alias: {
        react: path.resolve(__dirname, 'node_modules/react')
      }
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
          'process.env': {
            'API_HOST': JSON.stringify(process.env.API_HOST),
            'API_PORT_FRONT': JSON.stringify(process.env.API_PORT_FRONTEND),
            'API_PROTOCOL': JSON.stringify(process.env.API_PROTOCOL),
            'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'TESTING': JSON.stringify(process.env.TESTING)
          }
        }),

        new HtmlWebpackPlugin({
          title: '',
          filename: '../index.html',
          template: '../src/static/index.template.ejs',
          hash: false
        }),

        new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          minChunks: function (module) {
            return module.context && module.context.indexOf('node_modules') > -1;
          }
        })
    ],

    devServer: {
        contentBase: __dirname + '/src',
        host: 'localhost',
        port: 3000,
        historyApiFallback: true,
        hot: true
    },

    module: {
        rules: [
            {
              test: /\.js?$/,
              exclude: /node_modules/,
              loaders: ['babel-loader', 'eslint-loader']
            },
            {
              test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
              loader: 'url-loader?limit=100000'
            },
            {
              test: /(\.scss)|(\.css)$/,
              exclude: /(src)/,
              use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 2,
                        }
                    },
                    'sass-loader'
              ]
            },
            {
              test: /\.css$/,
              exclude: /node_modules/,
              use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 2,
                        }
                    },
                    'sass-loader',
                    'postcss-loader'
              ]
            }
        ]
    },

    externals: {
      appenders: {},
      child_process: {},
      cluster: {},
      defaultable: {},
      dgram: {},
      fs: {},
      module: {},
      logger: {},
      smtp: {}
    },

    devtool: "inline-source-map"
};

module.exports = config;
