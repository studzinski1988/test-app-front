/* eslint-disable */
var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var config = {
    context: __dirname + '/src',

    entry: {
      index: ['babel-polyfill', '../src/index.js']
    },

    output: {
        path: __dirname + '/dist/static',
        filename: '[name].bundle.js',
        sourceMapFilename: '[name].bundle.map',
        chunkFilename: '[name].bundle.js',
        publicPath: '/static/'
    },

    plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'API_HOST': JSON.stringify(process.env.API_HOST),
            'API_PORT_FRONT': JSON.stringify(process.env.API_PORT_FRONT),
            'API_PROTOCOL': JSON.stringify(process.env.API_PROTOCOL)
          }
        }),

        new HtmlWebpackPlugin({
          title: '',
          filename: '../index.html',
          template: '../src/static/index.template.ejs',
          hash: false,
          inject: false
        }),

        new CopyWebpackPlugin([
            { from: 'static/' },
        ], {
            ignore: [
                '*.ejs'
            ],
            copyUnmodified: true
        }),

        new webpack.optimize.CommonsChunkPlugin({
          name: 'vendor',
          minChunks: function (module) {
            return module.context && module.context.indexOf('node_modules') !== -1;
          }
        })
    ],

    module: {
        rules: [
            {
              test: /\.js?$/,
              exclude: /node_modules/,
              loaders: ['babel-loader']
            },
            {
              test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/,
              loader: 'url-loader?limit=100000'
            },
            {
              test: /(\.scss)|(\.css)$/,
              exclude: /(src)/,
              use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 2,
                        }
                    },
                    'sass-loader'
              ]
            },
            {
              test: /\.css$/,
              exclude: /node_modules/,
              use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 2,
                        }
                    },
                    'sass-loader',
                    'postcss-loader'
              ]
            }
        ]
    },

    externals: {
      module: {},
      child_process: {},
      fs: {},
      dgram: {},
      cluster: {},
      defaultable: {},
      logger: {},
      smtp: {},
      appenders: {}
    },

    bail: true,

    devtool: (process.env.NODE_ENV === 'development' ? 'source-map' : '' )
};

module.exports = config;
