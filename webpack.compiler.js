const webpack = require('webpack');
const onlineConfig = require('./webpack.online.config');
const compiler = webpack(onlineConfig);

console.log('Webpack compiler is running...');

const cb = (err, stats) => {
  if (err) {
    throw err;
  }
  console.log(stats);
  console.log('Compiled time: ', Math.round((stats.endTime - stats.startTime) / 1000), 'seconds');
};

compiler.run(cb);
